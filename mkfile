help:QV: ## show this command
	awk 'BEGIN {FS = "\t|:.*?## "};
	/[ \t]##[ \t]/ {printf "\033[36m%-20s\033[0m %s\n", $1, $NF}' \
		mkfile \
	| sort

requirements.txt:
	. .venv/bin/activate
	python -m pip freeze > requirements.txt

start:QV: ## setup working environment
	virtualenv .venv
	. .venv/bin/activate
	python -m pip install -r requirements.txt

ANSIBLE_ARGS="-vvv"

s6:V:	s6-rc/login.s6-rc ## build latest s6-rc db

s6-rc/login.s6-rc:	login/
	s6-rc-compile \
		-h $(whoami) \
		${target}.$(git rev-parse --short HEAD) \
		${prereq} \
	&& s6-ln -sf \
		login.s6-rc.$(git rev-parse --short HEAD) \
		${target}

install-dwm:QV: ## install skarnet tools in localhost
	. .venv/bin/activate
	ansible-playbook ${ANSIBLE_ARGS} ${target}.yml 2>&1 \
	| tee /dev/fd/2 \
	| s6-log -b n20 s1000000 t ./logs
